section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:                  
		cmp byte [rdi+rax], 0
		je .end               
		inc rax
		jmp .loop
	.end:
		ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdi, rsp
	call print_string
	pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push 0
    mov rax, rdi
    mov rdi, rsp
    mov rdx,0
    mov dl,0
    sub rsp, 50
    mov r8, 10
    dec rdi
.repoint:
    div r8
    add rdx, '0'
    dec rdi
    mov byte[rdi], dl
    mov rdx, 0
    cmp rax, 0
    jne .repoint
    call print_string
    add rsp, 58
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jl .change_sign
	jmp print_uint
.change_sign:
	neg rdi
	push rdi
	mov rdi, 0x2D
	call print_char
	pop rdi
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r10, r10
.check:
    mov r8b, [rdi + r10]
    cmp r8b, [rsi + r10]
    jne .done
    inc  r10
    test r8, r8
    jne .check
    mov rax, 1
.done:    
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx

.check_rules:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0
    je .ok
    cmp rdx, 0
    jne .read_all_word
    cmp rax, 0x9
    je .check_rules
    cmp rax, 0xA
    je .check_rules
    cmp rax, 0x20
    je .check_rules

.read_all_word:
    cmp rdx, rsi
    je .exception
    cmp rax, 0x9
    je .ok
    cmp rax, 0xA
    je .ok
    cmp rax, 0x20
    je .ok
    mov byte[rdi+rdx], al 
    inc rdx
    jmp .check_rules    
 
.ok:
    mov byte[rdi+rdx], 0 
    mov rax, rdi
    ret

.exception:
    xor rax, rax
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10


.digit:
    mov r10b, [rdi + rdx] 
	sub r10, 0x30 
	cmp r10, 0
	jnae .done
	cmp r10, 9
	jnbe .done
	inc rdx
	imul rax, 10 
	add rax, r10
	jmp .digit

.done:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor r10, r10
	mov r10b, [rdi]
	cmp r10, 0x2D
	je .change_sign
	jmp parse_uint
.change_sign:
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    inc rdi
    call parse_uint
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9
.next_char:
    cmp byte [rdi+rax], 0
    je .copy
    inc rax
    jmp .next_char
.copy:
    cmp rax, rdx
    jg .exception
    mov rcx, 0
.loop:
    mov r9b, byte [rdi+rcx]
    mov byte [rsi+rcx], r9b
    cmp rcx, rax
    je .done
    inc rcx
    jmp .loop


.exception:
    mov rax, 0
.done:
    ret
